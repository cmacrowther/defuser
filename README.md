<div  align="center">
 <h2>Defuser Website</h2>
</div>

<div  align="center">
[https://defuserband.com/](https://defuserband.com/)
</div>
<div  align="center">
Source code for my band Defuser's website<br/><br/>
![Netlify Status](https://api.netlify.com/api/v1/badges/c362e859-e9cf-4ac6-8c1e-3ec8f36e564c/deploy-status)
</div>

## Authors
I am the main contributer for this project, [@cmacrowther]() - Colin Crowther

## Copyright
© 2019 Colin Crowther
