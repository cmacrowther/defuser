---
title: Social Media
date: 2018-12-11 19:16:08 -0800

---
If you want to stay up to the minute on all things Defuser, be sure to head on over to our Instagram and Facebook pages and give them a like! We update these accounts with announcements, band updates and anything music related!

instagram.com/defuserband

facebook.com/defuserband